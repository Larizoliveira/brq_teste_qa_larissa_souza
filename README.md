Para executar os testes é necessário fazer o download do ruby no link abaixo:
https://rubyinstaller.org/downloads/
Após o download, inclua o ChromeDriver dentro da pasta C:\Ruby25-x64\bin do ruby
    link para download do chrome driver: https://chromedriver.chromium.org/downloads
Em seguida,abra o terminal e digite o seguintes comandos "gem install bundle" "gem install pretest" "gem install site-prism" 
Feito isso, entre dentro da pasta do projeto e execute o comando "bundle install", esse comando deverá instalar todas as gems necesáarias do ruby para executar o teste. 
Para executar o teste, digite "cucumber" que irá rodar todos os cenários do projeto.
Caso queira executar um cenário em especifíco digite  "cucumber -t @tagdocenário"
Eu inclui nos cenários as tags para facilitar a execução.
Fiz dois cenários (1- Primeira compra / 2- com um usuário já existente) "cucumber -t @PrimeiraCompra" "cucumber -t @usuarioExistente"
Caso for rodar mais de uma vez o cenário de primeira compra, deverá realizar a alteração do email no arquivo.BRQ_TESTE_QA_LARISSA_SOUZA\features\support\pages\cadastro_page.rb

Fico à disposição para esclarecimento de dúvidas!

Larissa Oliveira Souza 16/09/2020
email larissa.oliveira2107@gmail.com
Telefone 11 94497-5931