#language:pt
#utf-8


Funcionalidade: realizar compra no E-commerce com usuario já existente
#usuario já existente no E-commerce
Contexto: 
Dado que acessei o E-commerce
Quando seleciono o produto desejado

@RealizarCompra @usuarioExistente @teste @QA
Cenário: realizar compra no E-commerce 

E faço meu login no site
Então efetuei a minha compra
