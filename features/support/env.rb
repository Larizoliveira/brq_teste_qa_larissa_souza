require 'rspec'
require 'yaml'
require 'pry'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'site_prism'
require 'page-object'
require 'capybara'
require 'report_builder'


Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  #config.app_host =''
  config.default_max_wait_time = 5
end



