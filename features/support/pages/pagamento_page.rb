class PagamentoPageObject < SitePrism::Page
    
    #Metodo criado para selecionar forma de pagamento como Bank-wire
    def selecionar_metodo_pagamento_bw
        #Selecionar Forma de pagamento como Bank-wire
        page.find(:xpath,'//*[@id="HOOK_PAYMENT"]/div[1]/div/p/a').click
    end
    
    #Metodo criado para validar o valor total da compra
    def validar_valorTotal
        #Se necessario alterar o valor da compra
        page.has_text?("$29.00")
    end

    #Metodo criado para confirmar o pedido e avançar para a proxima etapa
    def confirmar_pedido
        #Clicar no botão submit 
        page.find(:xpath,'//*[@id="cart_navigation"]/button').click
    end


    #Metodo criado para validar se a forma de pagamento está correta    
    def confirmar_formaDePagamento
        page.has_text?("BANK-WIRE PAYMENT.")
    end

end