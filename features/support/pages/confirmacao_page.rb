class ConfirmacaoPageObject < SitePrism::Page

    #Metodo criado para validar se o pedido foi concluido com sucesso validando a forma de pagamento e valor da compra
    def confirmar_pedido
        #Valida se esta na pagina de confirmação do pedido
        page.has_text?("ORDER CONFIRMATION")
        #Valida a forma de pagamento do pedido
        page.has_text?("bank wire")
        #Valida o valor do pedido
        page.has_text?("$29.00")
    end

end
