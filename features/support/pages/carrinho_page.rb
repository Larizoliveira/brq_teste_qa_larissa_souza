class CarrinhoPageObject < SitePrism::Page
    #Metodo criado para seguir para a proxima etapa da compra no carrinho
    def prosseguir_compra
        #Botão para prosseguir para proxima etapa
        page.find(:xpath,  '//*[@id="center_column"]/p[2]/a[1]').click 
    end
    #Metodo criado para confirmar se o endereço inserido está correto
    def confirmar_endereco
        #Validar se o endereço está correto
        page.has_text?("Rua Bom Jesus Numero 1151")
    end

    #Metodo criado para aceitar os termos de privacidade do site e prosseguir para a proxima etapa
    def confirmar_compra
        #Campo para aceitar os termos de privacidade do site
        page.find('#uniform-cgv').click
        #Botão para prosseguir para proxima etapa
        page.find(:xpath,'//*[@id="form"]/p/button').click 
    end

    #Metodo criado para confimar se o produto incluido no carrinho está correto
    def confirmar_produto
        #**Alterar o texto caso, alterar o produto**
       page.has_text?('Blouse')
    end

    #Metodo criado para utilizar os metodos já criado, para ficar mais limpo o codigo
    def concluir_compra_cart
        @Carrinho = CarrinhoPageObject.new
        @Pagamento = PagamentoPageObject.new
        sleep(10)
        @Carrinho.confirmar_endereco
        @Carrinho.confirmar_compra
        @Pagamento.selecionar_metodo_pagamento_bw
        @Pagamento.confirmar_formaDePagamento
        @Pagamento.validar_valorTotal
        @Pagamento.confirmar_pedido
    end

end
