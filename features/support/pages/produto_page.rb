class ProdutoPageObject < SitePrism::Page

    element :addcart_btn, '#product'
    element :conf_addcart, :xpath, "//*[@id='layer_cart']/div[1]/div[2]/div[4]/a"

    #Metodo criado para adicionar o produto no carrinho
    def adicionar_carrinho
        #Clicar no botão ADD Cart
        page.find('#add_to_cart').click
        sleep(5)
        page.find(:xpath, "//*[@id='layer_cart']/div[1]/div[2]/div[4]/a").click
    end


   #Metodo criado para utilizar os metodos já criado, para ficar mais limpo o codigo
   def selecionar_Produto
        @Produto =ProdutoPageObject.new
        @Carrinho = CarrinhoPageObject.new
        sleep(5)
        @Produto.adicionar_carrinho
        @Carrinho.confirmar_produto
        @Carrinho.prosseguir_compra
    end

end