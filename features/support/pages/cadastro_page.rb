class CadastroPageObject < SitePrism::Page

element :email_create, '#email_create'
element :sbm_btn, '#SubmitCreate'
element :genero ,'#uniform-id_gender2'

    # Esse metodo foi criado para preencher o email para cadastro
    def incluir_email_cadastro
        #Campo para incluir email para cadastro 
        ## **ALTERAR O EMAIL CADA VEZ QUE FOR RODAR O TESTE** ##  
        fill_in 'email_create', :with => 'teste@testando8290.com'
        page.find('#SubmitCreate').click
    end

    # Esse metodo foi criado para preencher os campos dos dados pessoais do formulário de cadastro de primeira compra
    def dados_pessoais_cadastro
        #Selecionar Genero Feminino
        page.find('#uniform-id_gender2').click
        #Selecionar Genero Masculino *Descomentar caso desejar selecionar genero masculino*
        #page.find('#uniform-id_gender1').click
        #Campo para incluir primeiro nome desejado
        find("#customer_firstname").set("Larissa")
        #Campo para incluir sobrenome
        find("#customer_lastname").set("Souza")
        #Campo para incluir senha 
        find("#passwd").set("teste123")
        #Campo para incluir dia de nascimento
        page.find("#uniform-days").select("21")
        #Campo para incluir mês de nascimento
        page.find("#uniform-months").select("July")
        #Campo para incluir ano do nascimento
        page.find("#uniform-years").select("1996")
    end

    # Esse metodo foi criado para preencher os campos de endereço do cadastro
    def endereco_cadastro
        #Campo para incluir endereço
        page.find('#address1').set("Rua Bom Jesus")
        #Campo para incluir complemento do endereço
        page.find('#address2').set("Numero 1151 apto 84B")
        #Campo para incluir cidade
        page.find('#city').set("Sao Paulo")
        #Campo para incluir estado
        page.find("#uniform-id_state").select("New York")
        #Campo para incluir codigo postal
        page.find('#postcode').set("00000")
        #Campo para incluir telefone
        page.find('#phone_mobile').set("+5511944975931")
    end
    # Esse metodo foi criado para finalizar o codastro
    def submit_btn_cadastro
        #Campo para concluir cadastro e avançar para a proxima etapa
        page.find('#submitAccount').click
        #Botão para avançar para a proxima etapa
        page.find(:xpath,'//*[@id="center_column"]/form/p/button').click
    end

    #Metodo criado para utilizar os metodos já criado, para ficar mais limpo o codigo
    def efetuar_cadastro
        @Cadastro = CadastroPageObject.new
        @Cadastro.incluir_email_cadastro
        @Cadastro.dados_pessoais_cadastro
        @Cadastro.endereco_cadastro
        @Cadastro.submit_btn_cadastro      
    end


end