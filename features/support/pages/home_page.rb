class HomePageObject < SitePrism::Page

    element :home_page, :xpath, '//*[@id="header_logo"]/a/img'
    element :produto, :xpath, ' //*[@id="homefeatured"]/li[2]/div/div[1]'

    #Metodo criado para validar se estou na home page
    def validar_home
        page.has_xpath?('//*[@id="header_logo"]/a/img')
    end
    
    #Metodo criado para selecionar o produto desejado na home
    def selecionar_produto_hm
        page.find(:xpath,'//*[@id="homefeatured"]/li[2]/div/div[1]/div/a[1]/img').click
    end
end