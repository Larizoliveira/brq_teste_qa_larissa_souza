#language:pt
#utf-8


Funcionalidade: realizar primeira compra no E-commerce
 #usuario não tem cadastro pré existente no E-commerce

Contexto: 
Dado que acessei o E-commerce

@RealizarCompra @PrimeiraCompra @teste @QA
Cenário: realizar compra no E-commerce 

Quando seleciono o produto desejado
E efetuo o cadastro no site
Então efetuei a minha compra


