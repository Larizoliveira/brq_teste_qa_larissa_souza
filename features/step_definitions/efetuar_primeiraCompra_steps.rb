Dado("que acessei o E-commerce") do
    @AcessarEcomm.load
    @Home.validar_home

end
  
Quando("seleciono o produto desejado") do
    @Home.selecionar_produto_hm
    @Produto.selecionar_Produto
end
  
Quando("efetuo o cadastro no site") do
    @Cadastro.efetuar_cadastro
    @Carrinho.concluir_compra_cart
end
  
Então("efetuei a minha compra") do
    @Confirmacao.confirmar_pedido
    sleep(30)
end